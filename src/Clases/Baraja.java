package Clases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Enums.palEnums;
import Enums.valorEnums;

public class Baraja {
	
    private List<Carta> cartas;
    private int indiceCartaActual;

    public Baraja() {
    	
        cartas = new ArrayList<>();
        
        for (palEnums pal : palEnums.values()) {
            for (valorEnums valor : valorEnums.values()) {
                cartas.add(new Carta(pal, valor));
            }
        }
        
        indiceCartaActual = 0;
    }

    public void barajar() {
    	
        Collections.shuffle(cartas);
        indiceCartaActual = 0;
    }

    public Carta sacarCarta() {
    	
        if (indiceCartaActual >= cartas.size()) {
            return null;
        }
        return cartas.get(indiceCartaActual++);
    }

	@Override
	public String toString() {
		return "Baraja [cartas=" + cartas +"]";
	}
    
   
}
