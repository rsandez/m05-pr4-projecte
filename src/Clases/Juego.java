package Clases;

import java.util.Scanner;

import Enums.palEnums;
import Enums.valorEnums;

public class Juego {
	private Baraja baraja;
	private Carta cartaActual;
	private Scanner scanner;

	public Juego() {
		baraja = new Baraja();
		baraja.barajar();
		cartaActual = baraja.sacarCarta();
	}

	public void jugarAdivinarCarta() {
	    scanner = new Scanner(System.in);
	    int opcion = 0;
	    do {
	        // Muestra el menú de opciones
	        System.out.println("1. Jugar");
	        System.out.println("2. Mostrar cartes");
	        System.out.println("3. Sortir");
	        opcion = scanner.nextInt();
	        
	        // Si el usuario selecciona la opción de jugar
	        if (opcion == 1) {
	            int contador = 0;
	            String entrada = "";
	            String[] partes;
	            scanner.nextLine();
	            int aux = 0;
	            
	            // Se pide al usuario que adivine la carta
	            while (contador < 3) {
	                System.out.print("Endevina la carta (ex. 'Set de Piques'): ");
	                entrada = scanner.nextLine();
	                partes = entrada.split(" de ");
	                
	                // Se comprueba si la entrada es válida
	                if (partes.length != 2) {
	                    System.out.println("Entrada incorrecta, intenta-ho de nou.");
	                    continue;
	                }
	                
	                valorEnums valor = null;
	                palEnums pal = null;
	                
	                try {
	                    // Se intenta crear una carta con la entrada del usuario
	                    valor = valorEnums.valueOf(partes[0].toUpperCase());
	                    pal = palEnums.valueOf(partes[1].toUpperCase());
	                } catch (IllegalArgumentException e) {
	                    // Si la entrada no es válida, se muestra un mensaje de error
	                    System.out.println("Entrada incorrecta, intenta-ho de nou.");
	                    continue;
	                }
	                
	                Carta intento = new Carta(pal, valor);
	                
	                if (intento.equals(cartaActual)) {
	                    // Si la carta es la correcta, se muestra un mensaje de felicitación
	                    System.out.println("Felicitats, has endevinat la carta!");
	                    break;
	                } else {
	                    // Si la carta no es la correcta, se indica cuántos intentos quedan
	                    contador++;
	                    System.out.println("Ho sento, aquesta no és la carta correcta.");
	                    System.out.println("Intent nº " + contador + " de 3");
	                }
	                
	                if (contador == 1) {
	                    // Si es el primer intento, se le ofrece una pista al usuario
	                    System.out.println("Vols una pista?  |  Si, No");
	                    String respuesta = scanner.nextLine();
	                    if (respuesta.equalsIgnoreCase("Si")) {
	                        // Si el usuario quiere una pista, se muestra el pal de la carta
	                        System.out.println("El Pal es: " + cartaActual.getValor() );
	                    } else {
	                        // Si el usuario no quiere una pista, se le da un intento extra pero solo es posible 1 vez
	                        
	                        aux++;
	                        if (aux <= 1) {
	                        	System.out.println("Premi!! Intent extra per no demanar una Pista");
	                        	contador--;
							}else {
								System.out.println("No hi ha mes premis :( , a la proxima reclama la pista");
							}
	                        
	                    }
	                }else if (contador >= 3) {
	                	System.out.println( " La carta era: "+ cartaActual);
						
					}
	            }
	        } else if (opcion == 2) {
	            // Si el usuario selecciona la opción de mostrar cartas, se muestra la baraja completa
	            System.out.println(baraja);
	        }else {
	        	System.out.println("error en opcion");
	        }
	    } while (opcion != 3);
	}


}



