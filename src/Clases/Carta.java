package Clases;

import Enums.palEnums;
import Enums.valorEnums;

public class Carta {
    private palEnums pal;
    private valorEnums valor;
 

	public Carta(palEnums pal, valorEnums valor) {
        this.pal = pal;
        this.valor = valor;
    }


	public palEnums getPal() {
        return pal;
    }

    public valorEnums getValor() {
        return valor;
    }

    public String toString() {
        return valor + " de " + pal;
    }
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carta other = (Carta) obj;
		return pal == other.pal && valor == other.valor;
	}
    
    
}

